section .data
  list DB 1,2,3,4
  string DB "ABC",0

section .text

global _start

_start:
  MOV bl, [string]
  MOV eax, 1
  INT 80h
