#!/bin/bash

# Check if a filename argument is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <filename>"
  exit 1
fi

# Assign the filename argument to a variable
filename="$1"

# Assemble the file
nasm -f elf -o "${filename}.o" "${filename}.s"
if [ $? -ne 0 ]; then
  echo "Assembly failed"
  exit 1
fi

# Link the file
ld -m elf_i386 -o "${filename}" "${filename}.o"
if [ $? -ne 0 ]; then
  echo "Linking failed"
  exit 1
fi

# Execute the file and capture the exit status
./"${filename}"
exit_status=$?

# Print the exit status
echo "Exit status: $exit_status"
