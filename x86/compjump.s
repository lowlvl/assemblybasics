section .data

section .text
global _start

_start:
  MOV eax,3
  MOV ebx,2

lornot:
  CMP ebx,eax
  JL less
  JMP end

less:
  SHL ebx,1
  JMP lornot

end:
  MOV eax,1
  INT 80h
