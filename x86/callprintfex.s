extern here
extern printf
extern exit

section .data
  msg DD "Hello!",0x0a
  msg2 DD "Test!",0x0a
  fmt DD "Out is: %s %s",10,0
section .text
global main

main:
  PUSH msg2
  PUSH msg
  PUSH fmt
  CALL printf
  PUSH 10
  PUSH 13
  CALL here
  PUSH eax
  CALL exit

