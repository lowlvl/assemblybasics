section .data
        numb1 DB 1
        numb2 DB 2

section .text
global _start

_start:
        MOV bh, [numb1]
        MOV ch, [numb2]
        MOV al, 1
        INT 80h

