# Lineup | Order | Pattern

1. Declare few different sections (`.text`, `.data`, `.bss`)
2. Tell programm where it supposed to start (`.global _start`)
3. Write instructions under the `_start` label

### Compile program

```bash
nasm -f elf -o first.o first.s
ld -m elf_i386 -o first first.o
```

### `gdb`

```gdb
layout asm
```

## [Work with a small data](./smalldata.s)

- Storing small data (DB) in extended (eax,ebx...) registers will maximize memory usage by things next to each other in the same 32-bit memory slot -> using the whole 32-bit register
- bx, ax... - sliced to half (using *16-bit* of the register)
- `bl/cl` - low end (8-bits) = 0x1/0x2 -> `ebx/ecx` = 0x1/0x2 1/2
- `bh/ch` - high end = 0x1/0x2 -> `ebx,ecx` = 0x100/0x200 256/512

## [Lists](lists.s)

>[!note] Null terminator
> **Helps divide slots of memory between 'variables'**
> (where one ends and next began)
> In list of *numbers* indicate EOL = -1 (or 0)
> In list of *strings* indicate EOL = `,0`

## [bss](blockss.s)
- reserve memory

>[!attention] Data can't moved directly to memory. (register first)

- `[num+3]` - movin 3 bites forward

##### In `.data` section `num2 DB 3 DUP(2)`
- write 2 into memory 3 times
- like *initializing default value* repeatedly
- `0x00020202`

## Arithmetic and logic operations

### [add and eflags](addadceflags.s)
#### *eflags*
- set of bits
- each bit representing flag
- flag gives information about operation that just run

```perl
(gdb) info registers eflags
eflags      0x206       [ PF IF ]
```

>[!note] ADD two registers and result is too big
> use `ADC`: add with carry
> need to consider if value too big for the register

#### [Substruction](substruct.s)

```perl
eax     0xfffffffe      -2
```
- `f`'s - result of continuing borrowing until reach the end
- `SF` - sign flag (1 - negative, 0 - positive)

#### [Multiplication](mulimul.s)

- automatically applying to `a` register
- cause it is *accumulator* (default destination to the operation)
- automatically expend register (e.g. `al->ax`) if value too big for the register

>[!attention] MUL & IMUL
> Unsigned and Signed multiplication

#### [Division](division.s)
- working similar to multiplication
- have `DIV` and `IDIV`
- reminder save in `edx` register

## Logical Operators

>[!attention] NOT
> - Negotion flips all bits (even small)
> `1110` become `-15` (?1111)

#### Mask

- Often involves using bitwise operations *to set, clear, or toggle specific bits*.
- using `AND` with a mask to clear specific bits or `OR` with a mask to set specific bits.

```asm
AND eax, 0xFFFFFFF0
```
- would clear the *lower 4 bits* of eax regardless of its initial value

## Shifting

- `SHR,1` is equivalent of dividing by 2
- `SHL,1` is equivalent of multiplication by 2
- `SAR/SAL` - arithmetic (assigned) shift (preserve sign bit)

## Comparison and jump

- [loop](loop.s)
- based on substruction with `eflags` registers
- `JE` and `JZ` basically gives the same result

## Float Point

- `MOVSS` - scalar single-precision floating point
- print in gdb: `p $xmm0.v4_float[0]`

>[!attention] Floating-Point Precision Issue

- for comparison: `UCOMISS`
- for jumps: `JE/JB/JA`

## Call Standart C Functions

- in order to work with gcc - `main:` function instead of `_start`
- `PUSH` on **stack**
- `printf(format,message)`

#### Compilin
```bash
nasm -f elf -o file.o file.s
gcc -no-pie -m32 file.o -o file
```
##### For linking external custom function:
- `gcc -no-pie -m32 file.o cfunc.c -o file`

## [systemcalls](syscalls.s)
- syscall 4 -> write
- `ssize_t write(int fd, const void buf[.count], size_t count);`
- file descriptor(fd) 1 = stdout
