section .data

section .text
global _start

_start:
    MOV eax, 1
    MOV ebx, 2
    ADD eax, ebx

    MOV cl, 0b11111111
    MOV dl, 0b0001
    ADD cl, dl
    ADC ch, 0

    MOV eax, 1
    INT 80h

