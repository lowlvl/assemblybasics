# Assembly Code Repository

This repository contains my collection of assembly code projects, scripts, and exercises, organized by platform and architecture. Each directory represents a different architecture or platform, allowing for an organized exploration of assembly across various environments.

## Usage

1. **Requirements**: Ensure you have the necessary assemblers and linkers for the specific platform (e.g., NASM for x86, `as` for ARM).
2. **Building**: Follow the build instructions provided in each subdirectory or file header.
3. **Execution**: Run the resulting binaries in an appropriate environment (e.g., QEMU for emulation, native environment for supported platforms).

## Contents

### x86

- [x86 notes](x86/x86notes.md)
- [Build and run script](x86/build_elf_i386_run.sh)

## Goal

This repository serves as a comprehensive reference and study guide for learning assembly language on multiple architectures. Each file includes comments to aid in understanding the purpose and function of the code.

#### Happy coding in assembly!
